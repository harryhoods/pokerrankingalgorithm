//
//  Ranker.cpp
//  PokerCpp
//
//  Created by Hari Prabhakaran on 2/12/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

#include "Ranker.h"
#include <string.h>
/* Ranker Class Implementation */

//Bubble sorting the 1-5 cards in the hand based sheerly on the value (no need to care about suit now).
void Ranker::sortHand(){
    int i,j,curRank,prevRank;
    Card *tmp;
    if(nOfCards){
        for(i=1;i<nOfCards;i++){
            for(j=i;j>0;j--){
                curRank=Hand[j]->getRank();
                curRank=(curRank==1)?14:curRank;
                curRank=(curRank==16)?0:curRank;

                prevRank=Hand[j-1]->getRank();
                prevRank=(prevRank==1)?14:prevRank;
                prevRank=(prevRank==16)?0:prevRank;
                
                if( curRank > prevRank){
                    tmp=Hand[j];
                    Hand[j]=Hand[j-1];
                    Hand[j-1]=tmp;
                }
            }
        }
    }
}

void Ranker::printHand(){
    int i;
    if(nOfCards){
        std::cout<<" nOfCards:"<<nOfCards<<" ( ";
        for(i=0;i<nOfCards;i++){
            std::cout<<Hand[i]->getValue()<<" ";
            HandStrValue+=Hand[i]->getValue();
            HandStrValue+=" ";
        }
		std::cout<<")";
    }
}

Card* Ranker::pop(){
    Card *retVal=NULL;
    if(nOfCards)
        retVal=Hand[--nOfCards];
    return retVal;
}

bool Ranker::push(Card *C1){
    bool retVal = false;
    if(nOfCards<5){
        Hand[nOfCards++]=C1;
        retVal=true;
    }
    return retVal;
}

bool Ranker::removeCard(int n){
    bool retVal = true;
    int i;
    //if N is not the Last Element
    if(n>0){
        if(n<nOfCards){
            for(i=n-1;i<nOfCards-1;i++)
                Hand[i]=Hand[i+1];
        }
        if(!pop()) retVal=false;
    }
    else{}//Nothing to Delete. 'n' Needs to be greater than 0.
    // HAND Empty is the Only Legit Senario; this method returns FALSE.
    return retVal;
}

Ranker::Ranker(){
    int i;
    hRank=0.0;
	nOfCards=0;
	multiplier=0.01;
    for(i=0;i<5;i++){
        Hand[i] = NULL;
    }
}

Ranker::Ranker(string str){
    hRank=0.0;
	multiplier=0.01;
	nOfCards=0;
    std::stringstream strm(str);
    string tmp;
    Card *cobj;
    
    while(strm>>tmp){
        cobj=new Card(tmp);
        push(cobj);
    }
}

void Ranker::initHandStr(){
    int i;
    for(i=0;i<nOfCards;i++){
        HandStrValue+=Hand[i]->getValue();
        HandStrValue+=" ";
    }
}

Ranker::~Ranker(){
    free(this);
}

bool Ranker::isQuad(){
    bool retVal=false;
    int i,tmpValue;
    if(nOfCards>3){
        tmpValue=Hand[0]->getRank();
        tmpValue=(tmpValue==1)?14:tmpValue;
        tmpValue=(tmpValue==16)?0:tmpValue;
        if(tmpValue){
            retVal=true;
            for(i=1;i<4;i++){
                if(Hand[i]->getRank()!=Hand[i-1]->getRank()){
                    retVal=false;
                    break;
                }
            }
        }
        if(!retVal){
            tmpValue=Hand[nOfCards-1]->getRank();
            tmpValue=(tmpValue==1)?14:tmpValue;
            tmpValue=(tmpValue==16)?0:tmpValue;
            if(tmpValue){
                retVal=true;
                for(i=(nOfCards-2);i>=(nOfCards-4);i--){
                    if(Hand[i]->getRank()!=Hand[i+1]->getRank()){
                        retVal=false;
                        break;
                    }
                }
            }
            if(retVal){//The Quad is on the right hand side
                hRank=(double)7;
                hRank+=double(tmpValue)*multiplier;
                multiplier/=100;
                for(i=1;i<=4;i++) removeCard(nOfCards);
                //printHand();
            }
        }
        else{ //The Quad is on the left hand side
            hRank=(double)7;
            hRank+=double(tmpValue)*multiplier;
            multiplier/=100;
            for(i=1;i<=4;i++) removeCard(1);
            //printHand();
        }
    }
    return retVal;
}

bool Ranker::isFlush(){
    bool retVal=false;
    int i;
    if(nOfCards==5 && Hand[0]->getSuit()!=X){
        retVal=true;
        for(i=1;i<5;i++){
            if(Hand[i]->getSuit()!=Hand[i-1]->getSuit()){
                retVal=false;
                break;
            }
        }
        if(retVal) hRank=(double)5;
    }
    return retVal;
}

bool Ranker::isStraight(){
    //std::cout.precision(10);
    bool retVal=false;
    int i;
    //if 5 cards and first is not an Ace
    if(nOfCards==5){
        if(Hand[0]->getRank()!=(RANK)1){
            retVal=true;
            for(i=1;i<5;i++){
                if(Hand[i]->getRank()!=((Hand[i-1]->getRank())-1)){
                    retVal=false;
                    break;
                }
            }
        }
        else if(Hand[1]->getRank()==(RANK)5){
            //Checking Only Wheel (A,5,4,3,2) Senario
            retVal=true;
            for(i=2;i<5;i++){
                if(Hand[i]->getRank()!=((Hand[i-1]->getRank())-1)){
                    retVal=false;
                    break;
                }
            }
            if(retVal){
                //Rotating the Hand to form 5-4-3-2-A
                Card *ctmp=Hand[0];
                for(i=1;i<nOfCards;i++)
                    Hand[i-1]=Hand[i];
                Hand[i-1]=ctmp;
            }
            //printHand();
        }
        else if(Hand[1]->getRank()==(RANK)K){
            //Checking Only Broadway (A,K,Q,J,T) Senario
            retVal=true;
            for(i=2;i<5;i++){
                if(Hand[i]->getRank()!=((Hand[i-1]->getRank())-1)){
                    retVal=false;
                    break;
                }
            }
        }
        if(retVal){
            if((int)hRank==5)
                hRank+=3; //making it 8 -> Straight Flush
            else
                hRank=(double)4;
        }
    }
    return retVal;
}

bool Ranker::isSet(){
    bool retVal=false;
    int i,j,tmpValue;
    if(nOfCards>2){
        for(i=1;i<(nOfCards-1);i++){
            tmpValue=Hand[i-1]->getRank();
            tmpValue=(tmpValue==1)?14:tmpValue;
            tmpValue=(tmpValue==16)?0:tmpValue;
            if(tmpValue){
                retVal=true;
                for(j=i;j<(i+2);j++){
                    if( Hand[j]->getRank()!=Hand[j-1]->getRank() ){
                        retVal=false;
                        break;
                    }
                }
            }
            if(retVal){
                tmpValue=Hand[--j]->getRank();
                tmpValue=(tmpValue==1)?14:tmpValue;
                tmpValue=(tmpValue==16)?0:tmpValue;
                hRank=(double)3;
                hRank+=double(tmpValue)*multiplier;
                multiplier/=100;
                for(i=0;i<3;i++) removeCard(1+j--);
                //printHand();
                break;
            }
        }
    }
    return retVal;
}
bool Ranker::isPair(){
    bool retVal=false;
    int i,tmpValue;
    if(nOfCards>1){
        for(i=1;i<nOfCards;i++){
            
            if( Hand[i]->getRank()==Hand[i-1]->getRank() && Hand[i]->getRank()!=16){
                retVal=true;
                
                tmpValue=Hand[i]->getRank();
                tmpValue=(tmpValue==1)?14:tmpValue;
                tmpValue=(tmpValue==16)?0:tmpValue;
                hRank+=double(tmpValue)*multiplier;
                multiplier/=100;
                
                removeCard(i+1);
                removeCard(i);
                //std::cout<<"\nExiting inner Loop. j:"<<j<<" i:"<<i<<" nOfCards:"<<nOfCards<<std::endl;
                break;
            }
        }
    }
    if(retVal){
        if((int)hRank==3)
            hRank+=3; //Making it 6 -> Full House
        else if((int)hRank==1)
            hRank+=1; //Making it 2 -> 2Pair
        else
            hRank+=1; //a Pair
    }
    return retVal;
}

void Ranker::kicker( int noOfKicker){
    int i,tmpValue;
    std::cout.precision(11);
//  /*DEBUG*/std::cout<<"hrank:"<<hRank;    std::cout<<"Inside Kicker...\n"; printHand();
    if(nOfCards>0){
        for(i=0;(i<noOfKicker) && i<nOfCards;i++){
            tmpValue=Hand[i]->getRank();
            tmpValue=(tmpValue==1)?14:tmpValue;
            tmpValue=(tmpValue==16)?0:tmpValue;
            hRank+=double(tmpValue)*(multiplier);
            multiplier/=100;       
        }
        while(pop()); //Empty the Hand Stack
    }
}

double Ranker::handRank(){
	int noOfKickers=5; //default to 5 kickers.
    sortHand();

    if(isQuad()){
        noOfKickers=1;
    }
    else if(isFlush()){
        if(isStraight()){
			noOfKickers=1;
			//Straight Flush
        }
		//If only flush, we need all 5 kickers.
    }
    else if(isStraight()){
		noOfKickers=1;
    }
    else if(isSet()){
        if(isPair()){
			//Fullhouse
        }
		noOfKickers=2;
    }
    else if(isPair()){
        if(isPair()){
            noOfKickers=1;
        }
		noOfKickers=3;
    }
    kicker(noOfKickers);
    
    return hRank;
}