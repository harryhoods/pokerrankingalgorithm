//
//  Ranker.h
//  PokerCpp
//
//  Created by Hari Prabhakaran on 2/12/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//
/* Ranker Class Implementated in Ranker.cpp returns a double-float value for a 1-to-5 cards, a standard Poker Hand 
 */

#ifndef __PokerCpp__Ranker__
#define __PokerCpp__Ranker__

#include "Card.h"
#include <sstream>

class Ranker{
    short int nOfCards; //MAX 5
    Card *Hand[5];
    double hRank;
    double multiplier;
public:
    Ranker();
    Ranker(string str);
    ~Ranker();
    Card *pop();
    bool push(Card *C1);
    void sortHand();
    void printHand();
    double handRank();
    bool isQuad();
    bool isFlush();
    bool isStraight();
    bool isSet();
    bool isPair();
    void kicker(int n);
    bool removeCard(int n);
    string HandStrValue;
    void initHandStr();
};

#endif /* defined(__PokerCpp__Ranker__) */
