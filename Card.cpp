//
//  Poker.cpp
//  PokerCpp
//
//  Created by Hari Prabhakaran on 2/10/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

#include "Card.h"

/* Card Class Implementation */
Card::Card(){
    value = "";
}

Card::Card(RANK r,SUIT s){
    rank=r;
    suit=s;
    
    if(rank<(RANK)A || rank>(RANK)14)
        rank=RANK(Y);
    if(suit<0 || suit>3)
        suit=(SUIT)X;

    char tmp[3];
    memset(tmp,NULL,sizeof(tmp));
    sprintf(tmp,"%c%c",RANKCH[rank],SUITCH[suit]);
    value=tmp;
}

Card::Card(string str){
    int r;
    //char s;
    r= str[0]-'0';
    switch(str[0]){
        case 'A':
        case 'a':rank = (RANK)A; break;
        case 'T':
        case 't':rank = (RANK)T; break;
        case 'J':
        case 'j':rank = (RANK)J; break;
        case 'Q':
        case 'q':rank = (RANK)Q; break;
        case 'K':
        case 'k':rank = (RANK)K; break;
        default: rank = (RANK)r; break;
    }
    switch(str[1]){
        case 'S':
        case 's':suit = (SUIT)S; break;
        case 'H':
        case 'h':suit = (SUIT)H; break;
        case 'D':
        case 'd':suit = (SUIT)D; break;
        case 'C':
        case 'c':suit = (SUIT)C; break;
        default: suit = (SUIT)X; break;
    
    }
    
    if(rank<(RANK)A || rank>(RANK)14)
        rank=RANK(Y);
    if(suit<0 || suit>3)
        suit=(SUIT)X;

    char tmp[3];
    memset(tmp,NULL,sizeof(tmp));
    sprintf(tmp,"%c%c",RANKCH[rank],SUITCH[suit]);
    value=tmp;
 
}

Card::~Card(){
    free(this);
}

RANK Card::getRank(){
        return this->rank;
    }
    
SUIT Card::getSuit(){
        return this->suit;
    }
    
string Card::getValue(){
        return value;
    }



