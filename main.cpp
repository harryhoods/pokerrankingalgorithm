//
//  main.cpp
//  PokerCpp
//
//  Created by Hari Prabhakaran on 2/10/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

#include <iostream>
#include <time.h>
#include "Ranker.h"


int main(int argc, const char * argv[])
{
	unsigned long int startClock, endClock;
	int i;
	double handRank=0.0;
	unsigned short int highcard, nexthigh;
	char hcard, lcard;
	std::string HandDesc="", inpStr="";
	std::cout<<"\n=======================\n8-0 Poker Hand Ranking!\n=======================\n";
	
	if(argc<2){
		std::cout<<"\nPlease provide a hand value (1 to 5 cards)!\n\n***Usage   : 80ranking.exe [<card1> <card2> <card3> <card4> <card5>]\n***Example : 80ranking.exe As Kx T 8 J";
		std::cout<<"\n***For Demo: assigning a default value of Royal flush (As Ks Qs Js Ts)";
		inpStr="As Ks Qs Js Ts";
	}
	else 
		for(i=1;i<argc;i++){
		inpStr+=argv[i];
		inpStr+=" ";
	}
	//std::cout<<"\n"<<inpStr;
    
	std::cout.precision(10);// to print all 10 numbers after the decimal point
	Ranker *robj = new Ranker(inpStr);
    std::cout<<"\nCurrent Hand... ";
    robj->printHand();
    robj->sortHand();
    std::cout<<"\nAfter Sorting... ";
    robj->printHand();

/***** DEBUG (each case seperately) ***
    std::cout<<"\nIs Quad... "<<robj->isQuad();
    std::cout<<"\nIs Flush... "<<robj->isFlush();
    std::cout<<"\nIs Straight... "<<robj->isStraight();
    std::cout<<"\nIs Set... "<<robj->isSet();
    std::cout<<"\nIs Pair... "<<robj->isPair();
    std::cout<<"\nIs Pair... "<<robj->isPair();
    robj->kicker();
*/
	startClock=clock();
	handRank=robj->handRank();
	endClock=clock();

	highcard=(int)(handRank*100)%100;
	nexthigh=(int)(handRank*10000)%100;

	switch(highcard){
		case 14: hcard='A'; break;
		case 13: hcard='K'; break;
		case 12: hcard='Q'; break;
		case 11: hcard='J'; break;
		case 10: hcard='T'; break;
		default: hcard=(char)(highcard)+'0';  break;
	}	
	switch(nexthigh){
		case 14: lcard='A'; break;
		case 13: lcard='K'; break;
		case 12: lcard='Q'; break;
		case 11: lcard='J'; break;
		case 10: lcard='T'; break;
		default: lcard=(char)(nexthigh)+'0';  break;
	}
	//std::cout<<"\tHighcard: "<<highcard<<" hcard:"<<hcard<<"\t nexthigh"<<nexthigh<<" lcard:"<<lcard<<"\n";

	switch(int(handRank)){
		case 8: HandDesc=hcard; HandDesc+="-high Straight Flush"; break;
		case 7: HandDesc="Four-of-a-kind of "; HandDesc+=hcard; break;
		case 6: HandDesc="Full-House:"; HandDesc+=hcard;HandDesc+="s full of ";HandDesc+=lcard;HandDesc+="s"; break;
		case 5: HandDesc=hcard; HandDesc+="-high Flush"; break;
		case 4: HandDesc=hcard; HandDesc+="-high Straight"; break;
		case 3: HandDesc="Three-of-a-kind of "; HandDesc+=hcard; break;
		case 2: HandDesc="Two-Pair "; HandDesc+=hcard;HandDesc+="s and ";HandDesc+=lcard;HandDesc+="s"; break;
		case 1: HandDesc="Pair of "; HandDesc+=hcard; break;
		case 0: HandDesc="No-Pair ";HandDesc+=hcard;HandDesc+="-high"; break;
	}
	std::cout<<"\n\nHand Rank: "<<HandDesc<<"("<<handRank<<")"<<"!\n"; //"("<<handRank<<")\n";
	
	//destroy the object; free the pointer.
    free(robj);
	std::cout<<"\nTime taken:"<<(double)(endClock-startClock)/CLOCKS_PER_SEC<<" secs\n\n";
    return 0;
}
