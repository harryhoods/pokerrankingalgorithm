//
//  Poker.h
//  PokerCpp
//
//  Created by Hari Prabhakaran on 2/10/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

/* Constructs a basic Card with the properties;
 1.) 1<rank<15 (1 and 14 are Ace)
 2.) 0<suit<5 (0=spade, 1=diamond, 2=heart, 3=club, 4=X(unknown))
 3.) A string value representation of the Card like; As,7d,8h,2x,Kx,etc.
*/

#ifndef __PokerCpp__Poker__

#define __PokerCpp__Poker__
#include <iostream>
#include <string.h>
#include <stdlib.h>
using std::string;

typedef enum{
    S=0,
    H=1,
    D=2,
    C=3,
    X=4
}SUIT;

typedef enum{
    Y=16,
    A=1,
    T=10,
    J=11,
    Q=12,
    K=13
    //A is usually 1; 14 translates to "A" in the string value of the card.
}RANK;

const char RANKCH[17]={'0','A','2','3','4','5','6','7','8','9','T','J','Q','K','A',NULL,'Y'};
const char SUITCH[5]={'s','h','d','c','x'};

class Card{
    RANK rank;
    SUIT suit;
    string value;
public:
    Card();
    Card(RANK,SUIT);
    Card(string);
    ~Card();
    RANK getRank();
    SUIT getSuit();
    string getValue();
};



#endif /* defined(__PokerCpp__Poker__) */
